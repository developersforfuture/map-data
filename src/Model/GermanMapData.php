<?php

namespace Developers\FFuture\Map\Data\Model;

use Webmozart\Assert\Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class GermanMapData
{
    /**
     * Date in a string representation in YYYY-MM-DD format.
     *
     * @var string
     */
    protected $date;
    /**
     * A valid time representation as a string.
     *
     * @var string
     */
    protected $time;
    /**
     * @var string
     */
    protected $city;
    /**
     * A string representation for the location in the city.
     *
     * @var string
     */
    protected $location;
    /**
     * @var string
     */
    protected $hash;

    public function __construct(string $date, string $city, string $location, string $time)
    {
        $this->date = $date;
        $this->city = $city;
        $this->location = $location;
        $this->time = $time;
        $this->hash = md5($city.'|'.$location);
    }

    public static function fromCSVDataSet(array $data): self
    {
        Assert::count($data, 13);
        Assert::string($data[2]);
        Assert::notEmpty($data[2], 'The date string must be given.');
        $dateTime = date_create_from_format('d/m/Y', $data[2], new \DateTimeZone('Europe/Berlin'));
        if (!$dateTime) {
            var_dump($data);
            throw new \Exception('Invalid Date format "'.$data[2].'" please use "DD/MM/YYYY" as usual');
        }
        $date = $dateTime->format('Y-m-d');
        Assert::string($data[3]);
        Assert::notEmpty($data[2], 'The city string must be given.');
        $city = $data[3];
        Assert::string($data['4']);
        Assert::notEmpty($data[2], 'The date time must be given.');
        $time = $data[4];
        Assert::string($data[5]);
        Assert::notEmpty($data[2], 'The date location must be given.');
        $location = $data[5];

        return new self($date, $city, $location, $time);
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }
}
