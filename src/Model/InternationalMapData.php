<?php

namespace Developers\FFuture\Map\Data\Model;

use Webmozart\Assert\Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class InternationalMapData
{
    const FREQUENZY_NONE = 'None';
    const FREQUENZY_ONCE = 'Once only';
    const FREQUENZY_WEEKLY = 'Weekly';
    const FREQUENZY_MONTHLY = 'MONTHLY';
    const FREQUENZY_EVERY_FRIDAY = 'Every Friday';
    /**
     * Date in a string representation in YYYY-MM-DD format.
     *
     * @var string
     */
    protected $date;
    /**
     * A valid time representation as a string.
     *
     * @var string
     */
    protected $time;
    /**
     * @var string
     */
    protected $city;
    /**
     * A string representation for the location in the city.
     *
     * @var string
     */
    protected $location;
    /**
     * @var string
     */
    protected $hash;
    /**
     * @var string
     */
    private $frequenzy;

    public function __construct(string $date, string $city, string $location, string $time, string $frequenzy)
    {
        $this->date = $date;
        $this->city = $city;
        $this->location = $location;
        $this->time = $time;
        $this->hash = md5($city.'|'.$location);
        $this->frequenzy = $frequenzy;
    }

    public static function fromCSVDataSet(array $data): self
    {
        Assert::count($data, 12);

        Assert::string($data[10]);

        $frequenzy = isset($data[10]) ? $data[10] : self::FREQUENZY_NONE;
        Assert::string($data[9]);
        Assert::notEmpty($data[9], 'The date string must be given.');
        $date = $data[9];
        Assert::string($data[6]);
        Assert::notEmpty($data[6], 'The city string must be given.');
        $city = $data[6];
        Assert::string($data['8']);
        Assert::notEmpty($data[8], 'The time string must be given.');
        $time = $data[8];
        Assert::string($data[7]);
        Assert::notEmpty($data[7], 'The location string must be given.');
        $location = $data[7];

        return new self($date, $city, $location, $time, $frequenzy);
    }

    public static function fromGermanMapData(GermanMapData $data, $frequenzy): self
    {
        Assert::string($data->getCity());
        Assert::string($data->getLocation());
        Assert::string($data->getDate());
        Assert::string($data->getTime());

        return new self($data->getDate(), $data->getCity(), $data->getLocation(), $data->getTime(), $frequenzy);
    }

    public function toArray(): array
    {
        return [
            '',
            '',
            '',
            '',
            '',
            'Germany',
            $this->getCity(),
            $this->getLocation(),
            $this->getTime(),
            $this->getDate(),
            $this->getFrequenzy(),
            '',
        ];
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return string
     */
    public function getFrequenzy(): string
    {
        return $this->frequenzy;
    }
}
