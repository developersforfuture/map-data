<?php

require_once __DIR__.'/../vendor/autoload.php';

use Developers\FFuture\Map\Data\Model\GermanMapData;
use Developers\FFuture\Map\Data\Model\InternationalMapData;

$GERMAN_FILE=__DIR__.'/../data/german.csv';
$CONTROL_FILE = __DIR__.'/../data/control_data.csv';
$OUTPUT_FILE = __DIR__.'/../data/output.csv';

$row = 2;
$existingDataByDateAndHash = [];
if (($handle = fopen($CONTROL_FILE, "r"))) {
    while (($data = fgetcsv($handle, 2000))) {
        $row++;
        try {
            $model = InternationalMapData::fromCSVDataSet($data);
        } catch (\Exception $exception) {
            printf('[Error] in row %d: please have a look at the data: %s'.PHP_EOL, $row, $exception->getMessage());
            continue;
        }
        $existingDataByDateAndHash[$model->getDate()][$model->getHash()] = $model;
    }
}

printf('[Info] imported %s sets of international control data'.PHP_EOL, $row);
$row=2;
$germanMapData = [];
if (($handle = fopen($GERMAN_FILE, "r"))) {
    while (($data = fgetcsv($handle, 2000))) {
        $row++;
        try {
            $model = GermanMapData::fromCSVDataSet($data);
        } catch (\Exception $exception) {
            printf('[Error] in row %d: please have a look at the data: %s'.PHP_EOL, $row, $exception->getMessage());
            continue;
        }
        $germanMapData[$model->getDate()][$model->getHash()] = $model;
    }
}
printf('[Info] imported %s sets of german data'.PHP_EOL, $row);

/** @var InternationalMapData[] $asInternationalMapData */
$asInternationalMapData = [];
/**
 * @var GermanMapData $german;
 */
foreach ($germanMapData as $date => $perDate) {
    foreach ($perDate as $hash => $german) {
        if (isset($existingDataByDateAndHash[$date]) && isset($existingDataByDateAndHash[$date][$hash])) {
            printf(
                "There is still an entry in existing data for City: %s, Address: %s, Date: %s, Time: %s. It has frequenzy: %s at date %s in international data.\n",
                    $german->getCity(),
                    $german->getLocation(),
                    $german->getDate(),
                    $german->getTime(),
                    $existingDataByDateAndHash[$date][$hash]->getFrequenzy(),
                    $existingDataByDateAndHash[$date][$hash]->getDate()
                );
        }

        $asInternationalMapData[] = InternationalMapData::fromGermanMapData($german, InternationalMapData::FREQUENZY_ONCE);
    }
}

printf("New records to inser: %d", count($asInternationalMapData));
if (($handle = fopen($OUTPUT_FILE, "w"))) {
    foreach ($asInternationalMapData as $data) {
        fputcsv($handle, $data->toArray());
    }
}



